// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2022 Kunal Mehta <legoktm@debian.org>
//! MediaWiki SQL definitions for SeaQuery
//!
//! The `mwseaql` crate contains [MediaWiki SQL table definitions](https://www.mediawiki.org/wiki/Manual:Database_layout)
//! for use with [SeaQL's SeaQuery builder](https://docs.rs/sea-query/).
//!
//! ## Extensions
//! The following MediaWiki extension tables are supported:
//! * [Extension:Linter](https://www.mediawiki.org/wiki/Extension:Linter), under `linter` feature
//! * [Extension:PageAssessments](https://www.mediawiki.org/wiki/Extension:PageAssessments), under `page_assessments` feature
//! * [Extension:ProofreadPage](https://www.mediawiki.org/wiki/Extension:ProofreadPage), under `proofread_page` feature
//!
//! More extensions can and will be added upon request!
//!
//! ## Toolforge
//! Toolforge has [two special views to optimize queries](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Database#Tables_for_revision_or_logging_queries_involving_user_names_and_IDs), `logging_userindex` and `revision_userindex`.
//! A definition for the [`meta_p.wiki`](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Database#Metadata_database) table is also provided.
//! These definitions can be enabled using the `toolforge` feature.
//!
//! ## Contributing
//! `mwseaql` is a part of the [`mwbot-rs` project](https://www.mediawiki.org/wiki/Mwbot-rs).
//! We're always looking for new contributors, please [reach out](https://www.mediawiki.org/wiki/Mwbot-rs#Contributing)
//! if you're interested!
#![deny(clippy::all)]
#![cfg_attr(docs, feature(doc_cfg))]

pub use sea_query;

pub mod core;
#[cfg(feature = "linter")]
#[cfg_attr(docs, doc(cfg(feature = "linter")))]
pub mod linter;
#[cfg(feature = "page_assessments")]
#[cfg_attr(docs, doc(cfg(feature = "page_assessments")))]
pub mod page_assessments;
#[cfg(feature = "proofread_page")]
#[cfg_attr(docs, doc(cfg(feature = "proofread_page")))]
pub mod proofread_page;
#[cfg(feature = "toolforge")]
#[cfg_attr(docs, doc(cfg(feature = "toolforge")))]
pub mod toolforge;

pub use crate::core::*;
