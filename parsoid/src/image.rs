/*
Copyright (C) 2022 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//! Image-related code

use crate::{assert_element, clean_link, WikinodeIterator};
use kuchiki::NodeRef;
use urlencoding::decode;

/// Represents an image (`[[File:Foobar.jpg]]`)
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/2.4.0#Images) for more details.
#[derive(Debug, Clone)]
pub struct Image(pub(crate) NodeRef);

impl Image {
    // Could be mw:Image, mw:Image/Frameless, mw:Image/Thumb, mw:Image/Frame
    pub(crate) const TYPEOF_PREFIX: &'static str = "mw:Image";
    pub(crate) const SELECTOR: &'static str = "[typeof^=\"mw:Image\"]";

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }

    /// Get the MediaWiki page title corresponding to the image being embedded
    pub fn title(&self) -> String {
        for node in self.inclusive_descendants() {
            if let Some(element) = node.as_element() {
                if element.name.local == local_name!("img") {
                    return clean_link(
                        &decode(
                            element
                                .attributes
                                .borrow()
                                .get("resource")
                                .unwrap(),
                        )
                        .unwrap(),
                    );
                }
            }
        }
        panic!("Unable to find <img> tag inside Image node")
    }
}
