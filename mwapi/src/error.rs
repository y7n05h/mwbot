/*
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::fmt::{Display, Formatter};

#[derive(Clone, Copy, Debug)]
pub enum ErrorFormat {
    Html,
    Wikitext,
    Plaintext,
}

impl Display for ErrorFormat {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let val = match &self {
            ErrorFormat::Html => "html",
            ErrorFormat::Wikitext => "wikitext",
            ErrorFormat::Plaintext => "plaintext",
        };
        write!(f, "{}", val)
    }
}

impl Default for ErrorFormat {
    fn default() -> Self {
        Self::Plaintext
    }
}
