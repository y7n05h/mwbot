mwapi
=====
[![crates.io](https://img.shields.io/crates/v/mwapi.svg)](https://crates.io/crates/mwapi)
[![docs.rs](https://docs.rs/mwapi/badge.svg)](https://docs.rs/mwapi)
[![pipeline status](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/badges/main/pipeline.svg)](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/-/commits/main)
[![coverage report](https://gitlab.com/mwbot-rs/mwbot/badges/main/coverage.svg)](https://mwbot-rs.gitlab.io/mwbot/coverage/)

See the [full documentation](https://docs.rs/mwapi/) (docs for [main](https://mwbot-rs.gitlab.io/mwbot/mwapi/)).

## License
mwapi is (C) 2021 Kunal Mehta, released under the GPL v3 or any later version,
see COPYING for details.
