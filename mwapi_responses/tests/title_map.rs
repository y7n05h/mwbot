/*
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
use mwapi_responses::prelude::*;
use std::collections::HashMap;

mod test_client;

#[query(action = "query", redirects = "1")]
struct Response;

#[tokio::test]
async fn title_map() {
    let mut params = Response::params().to_vec();
    params.push(("titles", "Main_page|Albert einstein|Taylor_Swift"));
    let resp: Response = test_client::test(&params).await.unwrap();
    let expected: HashMap<_, _> = [
        // Normalized + redirect
        ("Main_page".to_string(), "Main Page".to_string()),
        // Only redirect
        ("Albert einstein".to_string(), "Albert Einstein".to_string()),
        // Only normalized
        ("Taylor_Swift".to_string(), "Taylor Swift".to_string()),
    ]
    .into_iter()
    .collect();
    assert_eq!(resp.title_map(), expected);
}
