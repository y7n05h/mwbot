/*
Copyright (C) 2020-2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::types::{ComplexType, RustType};
use serde::Deserialize;
use std::collections::BTreeMap;
use std::fs;

#[derive(Deserialize)]
#[cfg_attr(feature = "dbg", derive(Debug))]
pub(crate) struct Metadata {
    // Unused in Rust currently
    // pub(crate) name: String,
    // pub(crate) mode: String,
    pub(crate) fieldname: String,
    pub(crate) prop: Option<String>,
    pub(crate) fields: Vec<Field>,
    #[serde(default)]
    pub(crate) wrap_in_vec: bool,
}

impl Metadata {
    pub fn new(name: &str) -> Self {
        // XXX: Is loading JSON files the best way to store/access this metadata?
        let path =
            format!("{}/data/query+{}.json", env!("CARGO_MANIFEST_DIR"), name);
        let contents = fs::read_to_string(path).unwrap_or_else(|err| {
            panic!("Unable to load JSON metadata: {}: {}", name, err)
        });
        serde_json::from_str(&contents).unwrap_or_else(|err| {
            panic!("Unable to parse JSON metadata: {}: {}", name, err)
        })
    }

    /// Fields that match a specific prop
    pub fn get_fields(self, props: &[&str]) -> Vec<Field> {
        let mut found = vec![];
        for field in self.fields {
            if field.matches_prop(props) {
                found.push(field);
            }
        }
        found
    }
}

#[derive(Deserialize)]
#[cfg_attr(feature = "dbg", derive(Debug))]
pub(crate) struct Field {
    /// Name of field in API response
    pub(crate) name: String,
    /// Rust type to map to
    pub(crate) type_: RustType,
    /// prop= value that gives this field. The magic string
    /// "=default" can be used to indicate that the field
    /// is always available.
    pub(crate) prop: String,
    /// Value this field should be renamed to (e.g. "type" -> "type_")
    pub(crate) rename: Option<String>,
    /// Whether `#[serde(default)]` should be set on this field
    #[serde(default)]
    pub(crate) default: bool,
    /// If `#[serde(deserialize_with(...)]` should be used
    pub(crate) deserialize_with: Option<String>,
}

impl Field {
    fn matches_prop(&self, props: &[&str]) -> bool {
        self.prop == "=default"
            || self.prop.split("||").any(|x| props.contains(&x))
    }
}

/// Fields that are present in every action=query response
/// when titles=/pageids= is used
pub(crate) fn default_query_fields() -> BTreeMap<String, Field> {
    let fields = [
        // #[serde(default)]
        // ns: i32
        Field {
            name: "ns".to_string(),
            type_: RustType::Simple("i32".to_string()),
            prop: "=default".to_string(),
            rename: None,
            // Not present if title is invalid
            default: true,
            deserialize_with: None,
        },
        // title: String
        Field {
            name: "title".to_string(),
            type_: RustType::Simple("String".to_string()),
            prop: "=default".to_string(),
            rename: None,
            default: false,
            deserialize_with: None,
        },
        // pageid: Option<u32>
        Field {
            name: "pageid".to_string(),
            type_: RustType::Complex(ComplexType {
                inner: "u32".to_string(),
                vec: false,
                option: true,
            }),
            prop: "=default".to_string(),
            rename: None,
            default: false,
            deserialize_with: None,
        },
        // #[serde(default)]
        // missing: bool
        Field {
            name: "missing".to_string(),
            type_: RustType::Simple("bool".to_string()),
            prop: "=default".to_string(),
            rename: None,
            default: true,
            deserialize_with: None,
        },
        // #[serde(default)]
        // invalid: bool
        Field {
            name: "invalid".to_string(),
            type_: RustType::Simple("bool".to_string()),
            prop: "=default".to_string(),
            rename: None,
            default: true,
            deserialize_with: None,
        },
        // invalidreason: Option<String>
        Field {
            name: "invalidreason".to_string(),
            type_: RustType::Complex(ComplexType {
                inner: "String".to_string(),
                vec: false,
                option: true,
            }),
            prop: "=default".to_string(),
            rename: None,
            default: false,
            deserialize_with: None,
        },
    ];
    fields
        .into_iter()
        .map(|field| (field.name.to_string(), field))
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_metadata() {
        let info = Metadata::new("info");
        // 3 fields plus the default 9
        assert_eq!(info.get_fields(&["url"]).len(), 3 + 9);
    }

    #[test]
    fn test_field() {
        let field = Field {
            name: "test".to_string(),
            type_: RustType::Simple("String".to_string()),
            prop: "test1||test2".to_string(),
            rename: None,
            default: false,
            deserialize_with: None,
        };
        assert!(field.matches_prop(&["test1"]));
        assert!(field.matches_prop(&["test2"]));
        assert!(!field.matches_prop(&["test3"]));
    }

    #[test]
    fn test_default_query_fields() {
        let fields = default_query_fields();
        assert!(fields.contains_key("ns"));
    }
}
